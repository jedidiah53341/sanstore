<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\User;
use App\Category;

class ConfirmController extends Controller
{
    public function index()
    {
    	$order = Order::all();
        $product = Product::all();
        $category = Category::all();
        return view('admin.pesanan', compact('product','category','order'));
    }

    public function konfirmasipembayaran(Request $request, $order_id)
    {
    	$order = Order::all();
        $product = Product::all();
        $category = Category::all();
        $pesanan = Order::where('id', $order_id)->first();
        $pesanan->status = '2';
        $pesanan->update();
        return redirect('pesanan');
    }

}
