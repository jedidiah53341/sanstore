@extends('layouts.admin')

@section('content')

<style>

input, textarea {
	border:none;
	border-bottom:1px solid hsl(1,1%,90%);
	background:transparent;
	width:100%;
}
/* Start Style untuk tabel_kategori */
.tabel_kategori {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.tabel_kategori td, .tabel_kategori th {
  border: 1px solid #ddd;
  padding: 8px;
}


.tabel_kategori th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: hsl(220,100%,40%);
  color: white;
}

td > img {
	width:100%;
}

td input, td select {
	border:none;
}
/* End Style untuk tabel_kategori */

/*Start Input Modal */

.inputbutton {
	border:none;
	font-weight: :bold;
	margin:1%;
	padding:1%;
	border-radius:5px;
	color:white;
	background:hsl(220,100%,40%);
}
.inputbutton a {
	color:white;
	font-weight:bold;
}
.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 20px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 60%;
  position: relative;
  transition: all 5s ease-in-out;
  overflow: scroll;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

/* End Input Modal*/

</style>
<div class="container_kategori" style="padding:1%">
	<a class="nav-link float-right"style="text-decoration:none;" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i> Toggle Sidebar </a><br><br>
<h1>Daftar Produk</h1>
<button style="margin-left:0" class="inputbutton" ><a  href="#inputdatabaru">Input Item Baru</a></button>

<table class="tabel_kategori">
  <tr>
	<th>Nama Produk</th>
	<th>Kategori Produk</th>
	<th>Deskripsi kecil</th>
	<th>Deskripsi</th>
	<th>Jumlah stok</th>
	<th>Harga</th>
	<th>Gambar</th>
	<th>Aksi</th>
  </tr>
	@foreach($product as $produk)
	<tr>

<form action="/updateproduk/{{$produk->id}}" method="POST" enctype="multipart/form-data">
@csrf
@method('PUT')
<td><h5><input name="namaproduk" value="{{ $produk->name }}"></h5></td>


<td><h5><span class="badge badge-success">{{$produk->category->name}}</span></h5>
	<span>Update :</span>
	<select class="badge badge-info" name="kategoriproduk">

	@foreach($category as $kategori)
	<option value="{{$kategori->id}}" {{ $kategori->id == $produk->category_id ? 'selected' : '' }}>{{$kategori->name}}</option>
	@endforeach
</select>

</select></td>

<td><textarea name="deskripsikecilproduk" >{{ $produk->small_description }}</textarea></td>

<td><textarea name="deskripsiproduk">{{ $produk->description }}</textarea></td>

<td><input name="jumlahproduk" value="{{ $produk->quantity }}"></td>

<td><input name="hargaproduk" value="{{ $produk->price}}"></td>

<td style="width:10%;"><img src='images/product/{{ $produk->image }}'>
<label for="body">Update:<br></label>
			   <input class="w-100"type="file" name="gambarproduk" class="form-control">
</td>

<td>
<input  class="data inputbutton" type="submit" style="background:hsl(150,100%,30%);" value="Update Produk">
</form>

<form action="/hapusproduk/{{$produk->id}}" method="POST">
@csrf
@method('DELETE')
<input type="submit"  class="data inputbutton "  style="background:hsl(0,100%,20%);" value="Hapus Produk">
</form>
</td>

</tr>
@endforeach
</table>



<!-- Start Input Kategori Modal -->
<div id="inputdatabaru" class="overlay">
	<div class="popup">
		<h2 class="font-semibold text-xl text-gray-800 leading-tight" >Produk Baru</h2>
	<a class="close" href="#">&times;</a>
		<form action="buatprodukbaru" method="POST" enctype="multipart/form-data">
			@csrf

				<label for="title">Nama :</label>
				<input type="text"  name="namaproduk" id="title" placeholder="Masukkan nama produk">
				<br>
				<label>Kategori produk :</label><br>
				<select name="kategoriproduk" >
					@foreach($category as $kategori)
					<option value="{{$kategori->id}}">{{$kategori->name}}</option>
				@endforeach
			</select><br>
			<label>Deskripsi kecil produk</label><br>
			<textarea name="deskripsikecilproduk" ></textarea>

				<label for="body">Deskripsi produk:</label><br>
				<textarea name="deskripsiproduk"></textarea><br>
				<label>Jumlah tersedia</label><br>
				<input name="jumlahproduk" value=""><br>
				<label>Harga /item</label><br>
				<input name="hargaproduk" value=""><br>
				<label for="body">File gambar produk:<br></label>
			   <input class="w-100"type="file" name="gambarproduk" class="form-control">
			<br><br>
			<button type="submit" class="w-100 inputbutton">Tambah</button>
		</form>
	</div>
	</div>
</div>
<!-- End Input Kategori Modal -->

</div>
@endsection
