@extends('layouts.admin')

<style>
div {
	margin:0;
	padding:0;
}

.bootstraprows {
	margin-top:2%;
}
@media(max-width: 700px) {
.bootstraprows {

	height:auto;
}
.bootstraprows div {
	margin-top:3%;
}
}
</style>
@section('content')
<div class="p-2">

<div class="container">

	<div class="p-3 alert alert-primary =" role="alert">
  Selamat datang di dashboard, admin! Kelola seluruh aktivitas toko-mu dari sini.
  <a class="nav-link float-right"style="text-decoration:none;" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i> Toggle Sidebar </a><br><br>
</div>


 <div class="row bootstraprows">


    <div class="col-md">
<div class="btn btn-outline-primary w-100 text-left" role="alert">
    <h3><b>Confirm Invoices</b></h3>
    <p class="card-text">Anda punya {{$tobeconfirmedinvoicescount}} pesanan untuk dikonfirmasi. Cek sekarang!</p>
    @foreach ($tobeconfirmedinvoices as $p)
        <button type="button" class=" mb-2 btn btn-primary btn-sm">
{{$p->user->name}} <span class="badge badge-light">{{$p->updated_at}}</span>
</button>

    @endforeach
    <br>
    <a href="{{ url('pesanan') }}" class="btn btn-primary mt-2">Update Status Pesanan</a>
</div>
    </div>
 
     <div class="col-md">
<div class="btn btn-outline-danger w-100 text-left" role="alert">
    <h3><b>Product Restock</b></h3>
    <p class="card-text">Jangan sampai kehilangan pembeli karena kehabisan stock! Berikut produk-mu yang tersisa kurang dari 5 pcs :</p>
        @foreach ($productslessthanfive as $p)
    <button type="button" class="btn btn-danger btn-sm mb-2">
{{$p->name}} <span class="badge badge-light">Stock: {{$p->quantity}}</span>
</button>

    @endforeach
    <br>
    <a href="{{ url('dataproduct') }}" class="btn btn-danger mt-2">Update Produk</a>
</div>
    </div>

    <div class="col-md">
<div class="btn btn-outline-success w-100 text-left" role="alert">
    <h3><b>Daily income</b></h3>
    <p class="card-text">Berikut pendapatan penjualan anda hari ini :</p>
    <h4><b>Rp. {{ number_format($dailyincome, 2) }}</b></h4>
</div>
    </div>

  </div>



    <div class="row bootstraprows">


    <div class="col-md">
<div class="btn btn-outline-info w-100 text-left" role="alert">
    <h3><b>Bestseller</b></h3>
    <p class="card-text">Produk - produk ini paling dicari!</p>
    @foreach ($bestselling as $p)

    @endforeach
</div>
    </div>
 
     <div class="col-md">
<div class="btn btn-outline-info w-100 text-left" role="alert">
    <h3><b>Newest members</b></h3>
    <p class="card-text">Pelanggan berikut baru saja bergabung dengan toko anda :</p>
    @foreach ($newestmembers as $p)
        <button type="button" class=" mb-2 btn btn-info btn-sm">
{{$p->name}} <span class="badge badge-light">Mendaftar pada : {{$p->created_at}}</span>
</button>

    @endforeach
</div>
    </div>

    <div class="col-md">
<div class="btn btn-outline-success w-100 text-left" role="alert">
    <h3><b>Weekly income</b></h3>
    <p class="card-text">Berikut pendapatan penjualan anda selama seminggu ini :</p>
    <h4><b>Rp. {{ number_format($weeklyincome, 2) }}</b></h4>
</div>
    </div>

  </div>

</div>





</div>
@endsection
